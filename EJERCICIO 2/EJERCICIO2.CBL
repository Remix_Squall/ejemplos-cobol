000001*******************************************************************
      * PROGRAM ID: EJERCICIO-2                                         *
      *                                                                 *
      * OBJECTIVE: ESTE PROGRAMA REALIZA UN CONTROL DE NOTAS DE ALUMNOS *
      *            (CURSO, ID, ASIGNATURA Y NOTA) POR CURSO Y           *
      *            POSTERIORMENTE POR ALUMNO CON EL NUMERO DE           *
      *            ASIGNATURAS, SUSPENSOS Y SU MEDIA. AL FINAL SE       *
      *            MUESTRA UN TOTAL GENERAL DE APROBADOS TOTALES Y      *
      *            MEDIA TOTAL PONDERADA. EL FICHERO QUE CONTIENE       *
      *            ESTA INFORMACION ESTA ORDENADO POR CURSO E ID DEL    *
      *            ALUMNO.                                              *
      *                                                                 *
      * TYPE:      BATCH REPORT                                         *
      *                                                                 *
      * INPUTS:    CURSO~1 (CURSO-FICH.DAT)                             *
      *                                                                 *
      * OUTPUTS:   CURSO~2 (CURSO-INF.DAT)                              *
      *                                                                 *
      *******************************************************************
      *******************************************************************
      * IDENTIFICATION DIVISION                                         *
      *******************************************************************
       IDENTIFICATION DIVISION.
        PROGRAM-ID. EJERCICIO-2.
        AUTHOR. LUIS.
        DATE-WRITTEN. 5 DICIEMBRE, 2014.
        DATE-COMPILED. 10 DICIEMBRE, 2014.

000002*******************************************************************
      * ENVIRONMENT DIVISION                                            *
      *******************************************************************
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
      *SPECIAL-NAMES.
      *    DECIMAL-POINT IS COMMA.
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.

      *******************************************************************
      * INPUT FILE                                                      *
      * --------------------------------------------------------------- *
      * CURSO-FICH: FICHERO DE ENTRADA                                  *
      *******************************************************************
           SELECT CURSO-FICH
            ASSIGN TO DISK 'CURSO-~1.DAT'
            ACCESS MODE IS SEQUENTIAL
            ORGANIZATION IS SEQUENTIAL.

      *******************************************************************
      * OUTPUT FILE                                                     *
      * --------------------------------------------------------------- *
      * CURSO-INF: FICHERO DE SALIDA                                    *
      *******************************************************************
           SELECT CURSO-INF 
            ASSIGN TO DISK 'CURSO-~2.DAT'
            ORGANIZATION IS SEQUENTIAL
            ACCESS MODE IS SEQUENTIAL.

000002*******************************************************************
      * DATA DIVISION                                                   *
      *******************************************************************
       DATA DIVISION.
       FILE SECTION.
        FD CURSO-FICH
           LABEL RECORDS ARE OMITTED.
        01 REG-CURSO-FICH PIC X(17).

        FD CURSO-INF
           LABEL RECORDS ARE OMITTED.
        01 REG-CURSO-INF PIC X(75).

      *******************************************************************
      * WORKING-STORAGE SECTION                                         *
      *******************************************************************
       WORKING-STORAGE SECTION.
        01 ALUMNO.
           02 AL-CUR  PIC X(4).
           02 FILLER  PIC X VALUE SPACES.
           02 AL-ALU  PIC 9(2).
           02 FILLER  PIC X VALUE SPACES.
           02 AL-ASIG PIC 9(4).
           02 FILLER  PIC X VALUE SPACES.
           02 AL-NOT  PIC 9(2).

        01 ALUMNO-ANT.
           02 AL-CUR-ANT PIC X(4).
           02 FILLER PIC X VALUE SPACES.
           02 AL-ALU-ANT PIC 9(2).
           02 FILLER PIC X VALUE SPACES.
           02 AL-ASIG-ANT PIC 9(4).
           02 FILLER PIC X VALUE SPACES.
           02 AL-NOT-ANT PIC 9(2).

      *******************************************************************
      * SEPARADORES                                                     *
      *******************************************************************
        01 SEP-1 PIC X(75) VALUE ALL '='.
        01 SEP-2 PIC X(75) VALUE ALL '*'.

      *******************************************************************
      * BOOLEANOS                                                       *
      *******************************************************************
        01 SW-FIN-FICHERO PIC 9 VALUE ZERO.
           88 FIN-FICHERO VALUE 1.

      *******************************************************************
      * CABECERA INFORMATIVA                                            *
      *******************************************************************
        01 CURSO.
           02 FILLER PIC X(14) VALUE SPACES.
           02 FILLER PIC X(47) VALUE
            'E J E R C I C I O  2  -  POR LUIS ROMERO MORENO'.

      *******************************************************************
      * CABECERA CURSO                                                  *
      *******************************************************************
        01 CURSO-CABECERA.
           02 FILLER PIC X(6) VALUE 'CURSO:'.
           02 FILLER PIC X(4) VALUE SPACES.
           02 CU-CUR PIC X(4).
           02 FILLER PIC X(6) VALUE SPACES.
           02 FILLER PIC X(6) VALUE 'ALUMNO'.
           02 FILLER PIC X(9) VALUE SPACES.
           02 FILLER PIC X(9) VALUE 'ASIGNATUR'.
           02 FILLER PIC X(6) VALUE SPACES.
           02 FILLER PIC X(9) VALUE 'SUSPENSOS'.
           02 FILLER PIC X(6) VALUE SPACES.
           02 FILLER PIC X(5) VALUE 'MEDIA'.

      *******************************************************************
      * CABECERA DETALLE                                                *
      *******************************************************************
        01 CURSO-DETALLE.
           02 FILLER  PIC X(22) VALUE SPACES.
           02 DE-ALU  PIC 9(2).
           02 FILLER  PIC X(13) VALUE SPACES.
           02 DE-ASIG PIC 9(5).
           02 FILLER  PIC X(10) VALUE SPACES.
           02 DE-SUSP PIC 9(5).
           02 FILLER  PIC X(8)  VALUE SPACES.
           02 DE-NOT  PIC 99.99.

      *******************************************************************
      * CABECERA INFORME TOTAL                                          *
      *******************************************************************
        01 CURSO-TOTAL.
           02 FILLER
            PIC X(35) VALUE 'TOTAL Alumnos todas asignaturas AP:'.
           02 FILLER   PIC X(3)  VALUE SPACES.
           02 TO-APROB PIC 9(3).
           02 FILLER   PIC X(3)  VALUE SPACES.
           02 FILLER   PIC X(17) VALUE 'Nota Media curso:'.
           02 FILLER   PIC X(4)  VALUE SPACES.
           02 TO-MED   PIC 99.99.

      *******************************************************************
      * CONTADORES                                                   *
      *******************************************************************
        01 CONTADORES.
           02 CONT-ASIG        PIC 99999 COMP-3 VALUE ZEROS.
           02 CONT-SUSP        PIC 99999 COMP-3 VALUE ZEROS.
           02 CONT-APRO        PIC 999   COMP-3 VALUE ZEROS. 
           02 CONT-MEDIA       PIC 999V99 COMP-3 VALUE ZEROS.
           02 CONT-MEDIA-TOTAL PIC 999V99 COMP-3 VALUE ZEROS.
           02 CONT-ASIG-TOTAL  PIC 999   COMP-3 VALUE ZEROS.

000003**************************************
      * PROCEDURE DIVISION                 *
      **************************************
       PROCEDURE DIVISION.
        1000-PRINCIPAL.
            PERFORM 9000-ABRE-FICHERO
             THRU 9000-ABRE-FICHERO-EXIT.
            PERFORM 1100-ESCRITURA-ENUNCIADO
             THRU 1100-ESCRITURA-ENUNCIADO-EXIT.
            PERFORM 9200-LEE-LINEA
             THRU 9200-LEE-LINEA-EXIT.
            MOVE ALUMNO TO ALUMNO-ANT.
            PERFORM 2000-PROCESA-CURSO
             THRU 2000-PROCESA-CURSO-EXIT
              UNTIL FIN-FICHERO.
            PERFORM 9100-CIERRA-FICHERO
             THRU 9100-CIERRA-FICHERO-EXIT.
            STOP RUN.

      *******************************************************************
      * 1100-ESCRITURA-ENUNCIADO                                        *
      * --------------------------------------------------------------- *
      * ESCRIBE EN EL FICHERO EL NUMERO DEL EJERCICIO Y EL AUTOR DEL    *
      * PROGRAMA.                                                       *
      *******************************************************************
        1100-ESCRITURA-ENUNCIADO.
            WRITE REG-CURSO-INF FROM SEP-2.
            WRITE REG-CURSO-INF FROM CURSO
             AFTER ADVANCING 0 LINES.
            WRITE REG-CURSO-INF FROM SEP-2
             AFTER ADVANCING 0 LINES.

        1100-ESCRITURA-ENUNCIADO-EXIT.
            EXIT.

      *******************************************************************
      * 2000-PROCESA-CURSO                                              *
      * --------------------------------------------------------------- *
      * BUCLE QUE IMPRIME LA CABECERA DEL CURSO Y QUE CREA OTRO BUCLE   *
      * INTERNO, QUE POSTERIORMENTE IMPRIMIRÁ EL REPORTE FINAL.         *
      *******************************************************************
        2000-PROCESA-CURSO.
            MOVE AL-CUR TO CU-CUR.
            PERFORM 2100-ESCRITURA-CURSO
             THRU 2100-ESCRITURA-CURSO-EXIT.
            PERFORM 3000-PROCESA-ALUMNO
             THRU 3000-PROCESA-ALUMNO-EXIT
              UNTIL (NOT AL-CUR = AL-CUR-ANT)
              OR FIN-FICHERO.
            PERFORM 4100-ESCRITURA-REPORTE
             THRU 4100-ESCRITURA-REPORTE.
            MOVE ALUMNO TO ALUMNO-ANT.

        2000-PROCESA-CURSO-EXIT.
            EXIT.

      *******************************************************************
      * 2100-ESCRITURA-CURSO                                            *
      * --------------------------------------------------------------- *
      * ESCRIBE EN EL FICHERO DE SALIDA LA CABECERA DEL CURSO.          *
      *******************************************************************
        2100-ESCRITURA-CURSO.
            WRITE REG-CURSO-INF FROM SEP-1.
            WRITE REG-CURSO-INF FROM CURSO-CABECERA.
            WRITE REG-CURSO-INF FROM SEP-1.

        2100-ESCRITURA-CURSO-EXIT.
            EXIT.

      *******************************************************************
      * 3000-PROCESA-ALUMNO                                             *
      * --------------------------------------------------------------- *
      * BUCLE QUE ANALIZA LOS ALUMNOS PARA CREAR OTRO BUCLE INTERNO Y   *
      * POSTERIORMENTE ESCRIBIR LOS DETALLES EN EL FICHERO DE SALIDA.   *
      *******************************************************************
        3000-PROCESA-ALUMNO.
            INITIALIZE CONT-ASIG CONT-SUSP CONT-MEDIA.
            MOVE ALUMNO TO ALUMNO-ANT.
            PERFORM 4000-PROCESA-MEDIA
             THRU 4000-PROCESA-MEDIA-EXIT
             UNTIL (NOT AL-CUR = AL-CUR-ANT)
             OR (NOT AL-ALU = AL-ALU-ANT)
             OR FIN-FICHERO.
            PERFORM 3100-ESCRITURA-ALUMNO
             THRU 3100-ESCRITURA-ALUMNO-EXIT.

        3000-PROCESA-ALUMNO-EXIT.
            EXIT.

      *******************************************************************
      * 3100-ESCRITURA-ALUMNO                                           *
      * --------------------------------------------------------------- *
      * ESCRIBE LOS DETALLES DEL ALUMNO EN EL FICHERO DE SALIDA Y       *
      * REINICIA LOS CONTADORES.                                        *
      *******************************************************************
        3100-ESCRITURA-ALUMNO.
            COMPUTE CONT-MEDIA = CONT-MEDIA / CONT-ASIG.
            MOVE CONT-MEDIA TO DE-NOT.
            MOVE CONT-SUSP TO DE-SUSP.
            MOVE CONT-ASIG TO DE-ASIG.
            MOVE AL-ALU-ANT TO DE-ALU.
            IF DE-SUSP EQUAL TO 0
             ADD 1 TO CONT-APRO
            END-IF.
            WRITE REG-CURSO-INF FROM CURSO-DETALLE.
            INITIALIZE CONT-MEDIA CONT-SUSP CONT-ASIG.

        3100-ESCRITURA-ALUMNO-EXIT.
            EXIT.

      *******************************************************************
      * 4000-PROCESA-MEDIA                                              *
      * --------------------------------------------------------------- *
      * AÑADE VALORES A LOS CONTADORES EN EL BUCLE POR CADA ALUMNO Y    *
      * LEE LA SIGUIENTE LINEA.                                         *
      *******************************************************************
        4000-PROCESA-MEDIA.
            ADD 1 TO CONT-ASIG.
            ADD 1 TO CONT-ASIG-TOTAL.
            IF AL-NOT < 5
             ADD 1 TO CONT-SUSP
            END-IF.
            ADD AL-NOT TO CONT-MEDIA.
            ADD AL-NOT TO CONT-MEDIA-TOTAL.
            MOVE ALUMNO TO ALUMNO-ANT.
            PERFORM 9200-LEE-LINEA
             THRU 9200-LEE-LINEA-EXIT.

        4000-PROCESA-MEDIA-EXIT.
            EXIT.

      *******************************************************************
      * 4100-ESCRITURA-REPORTE                                          *
      * --------------------------------------------------------------- *
      * ESCRIBE EL REPORTE TOTAL DE LOS DATOS Y REINICIA LOS            *
      * CONTADORES.                                                     *
      *******************************************************************
        4100-ESCRITURA-REPORTE.
            COMPUTE 
             CONT-MEDIA-TOTAL = CONT-MEDIA-TOTAL / CONT-ASIG-TOTAL.
             MOVE CONT-MEDIA-TOTAL TO TO-MED.
             MOVE CONT-APRO TO TO-APROB.
             WRITE REG-CURSO-INF FROM SEP-1.
             WRITE REG-CURSO-INF FROM CURSO-TOTAL.
             WRITE REG-CURSO-INF FROM SEP-1.
             INITIALIZE CONT-ASIG-TOTAL CONT-MEDIA-TOTAL CONT-APRO.

        4100-ESCRITURA-REPORTE-EXIT.
            EXIT.

      *******************************************************************
      * 9000-ABRE-FICHERO                                               *
      * --------------------------------------------------------------- *
      * ABRE EL FICHERO DE ENTRADA (CURSO-FICH) Y SALIDA (CURSO-INF).   *
      *******************************************************************
        9000-ABRE-FICHERO.
            OPEN INPUT CURSO-FICH
             OUTPUT CURSO-INF.

        9000-ABRE-FICHERO-EXIT.
            EXIT.

      *******************************************************************
      * 9100-CIERRA-FICHERO                                             *
      * --------------------------------------------------------------- *
      * CIERRA EL FICHERO DE ENTRADA (CURSO-FICH) Y SALIDA (CURSO-INF). *
      *******************************************************************
        9100-CIERRA-FICHERO.
            CLOSE CURSO-FICH
             CURSO-INF.

        9100-CIERRA-FICHERO-EXIT.
            EXIT.

      *******************************************************************
      * 9200-LEE-LINEA                                                  *
      * --------------------------------------------------------------- *
      * LEE LINEA A LINEA EL FICHERO DE ENTRADA Y LA ALMACENA EN LA     *
      * VARIABLE ALUMNO.                                                *
      *******************************************************************
        9200-LEE-LINEA.
            READ CURSO-FICH INTO ALUMNO
             AT END
              SET FIN-FICHERO TO TRUE.

        9200-LEE-LINEA-EXIT.
            EXIT.