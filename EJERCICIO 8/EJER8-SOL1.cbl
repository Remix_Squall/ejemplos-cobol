       IDENTIFICATION DIVISION.
       PROGRAM-ID. ORDEN1.
       REMARKS. Ordenar el fichero VENTAS.

       ENVIRONMENT DIVISION.
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.
      
      *DefiniciOn del fichero a utilizar       
            SELECT E-VENTAS ASSIGN TO DISK "VENTAS.DAT"
                            ORGANIZATION IS LINE SEQUENTIAL.
            SELECT Trabajo  ASSIGN TO DISK.
            SELECT S-VENTAS ASSIGN TO DISK "SALIDA.DAT"
                            ORGANIZATION IS LINE SEQUENTIAL.       
       
       DATA DIVISION.
       FILE SECTION.
       FD  E-VENTAS 
           LABEL RECORD IS STANDARD
           RECORD CONTAINS 17 CHARACTERS
           DATA RECORD IS REG-E-VENTAS.
       01  REG-E-VENTAS.
           02 FILLER   PIC X(17).
       FD  S-VENTAS
           LABEL RECORD IS STANDARD
           RECORD CONTAINS 17 CHARACTERS
           DATA RECORD IS REG-S-VENTAS.
       01  REG-S-VENTAS.
           02 FILLER   PIC X(17).
       SD  TRABAJO 
           RECORD CONTAINS 17 CHARACTERS
           DATA RECORD IS REG-TRABAJO.
       01  REG-TRABAJO.
           02 ALMACEN PIC XXX.
           02 FILLER   PIC X(14).
       PROCEDURE DIVISION.
       
       P1.
            SORT TRABAJO
              ASCENDING KEY ALMACEN 
              USING  E-VENTAS
              GIVING S-VENTAS.
            STOP RUN.