       IDENTIFICATION DIVISION.
       PROGRAM-ID. ORDEN3.
       REMARKS. Ordenar el fichero VENTAS.

       ENVIRONMENT DIVISION.
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.
      
      *Definicion del fichero a utilizar       
            SELECT E-VENTAS ASSIGN TO DISK "BORRA.DAT"
                            ORGANIZATION IS LINE SEQUENTIAL.
            SELECT Trabajo  ASSIGN TO DISK.
       
       DATA DIVISION.
       FILE SECTION.
       FD  E-VENTAS 
           LABEL RECORD IS STANDARD
           RECORD CONTAINS 17 CHARACTERS
           DATA RECORD IS REG-E-VENTAS.
       01  REG-E-VENTAS.
           02 FILLER   PIC X(17).
       SD  TRABAJO 
           RECORD CONTAINS 17 CHARACTERS
           DATA RECORD IS REG-TRABAJO.
       01  REG-TRABAJO.
           02 ALMACEN PIC XXX.
           02 FILLER   PIC X(14).
       PROCEDURE DIVISION.
       
       P1.
            SORT TRABAJO
              DESCENDING KEY ALMACEN 
              USING  E-VENTAS
              GIVING E-VENTAS.
            STOP RUN.